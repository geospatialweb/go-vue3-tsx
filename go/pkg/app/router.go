package app

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/rs/cors"
)

func (a *App) createRouter() http.Handler {
	cors := createCors()
	handler := a.registerRoutes(createMux())
	return cors.Handler(handler)
}

func createCors() *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins: []string{"https://www.geospatialweb.ca", "http://localhost:4173", "http://localhost:5173"},
		AllowedHeaders: []string{"Accept", "Authorization", "Content-Type"},
		AllowedMethods: []string{"DELETE", "GET", "PATCH", "POST", "PUT", "OPTIONS"},
	})
}

func createMux() *chi.Mux {
	mux := chi.NewRouter()
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)
	return mux
}

func (a *App) registerRoutes(mux *chi.Mux) http.Handler {
	mux.Route(a.Env.ApiPathPrefix, func(r chi.Router) {
		r.Use(a.authRequired)
		r.Delete(a.Env.DeleteUserEndpoint, a.handler.DeleteUser)
		r.Get(a.Env.GeoJSONEndpoint, a.handler.GetGeoJSONFeatureCollection)
		r.Get(a.Env.MapboxAccessTokenEndpoint, a.handler.GetMapboxAccessToken)
		r.Get(a.Env.GetUserEndpoint, a.handler.GetUser)
		r.Patch(a.Env.UpdatePasswordEndpoint, a.handler.UpdatePassword)
	})
	mux.Route(a.Env.CredentialsPathPrefix, func(r chi.Router) {
		r.Get(a.Env.ValidateUserEndpoint, a.handler.GetUser)
		r.Get(a.Env.LoginEndpoint, a.handler.LoginUser)
		r.Post(a.Env.RegisterEndpoint, a.handler.RegisterUser)
	})
	return mux
}
