package app

import (
	"fmt"
	"net/http"
	"strings"
	"testing"

	"github.com/go-chi/chi/v5"
)

type route struct {
	name       string
	method     string
	requestURI string
}

func Test_App_Router(t *testing.T) {
	handler := a.registerRoutes(createMux())
	routes, _ := handler.(chi.Routes)
	expectedRoutes := []route{
		{"deleteUser", http.MethodDelete, fmt.Sprintf("%s%s", a.Env.ApiPathPrefix, a.Env.DeleteUserEndpoint)},
		{"getGeoJSON", http.MethodGet, fmt.Sprintf("%s%s", a.Env.ApiPathPrefix, a.Env.GeoJSONEndpoint)},
		{"getUser", http.MethodGet, fmt.Sprintf("%s%s", a.Env.ApiPathPrefix, a.Env.GetUserEndpoint)},
		{"getMapboxAccessToken", http.MethodGet, fmt.Sprintf("%s%s", a.Env.ApiPathPrefix, a.Env.MapboxAccessTokenEndpoint)},
		{"updatePassword", http.MethodPatch, fmt.Sprintf("%s%s", a.Env.ApiPathPrefix, a.Env.UpdatePasswordEndpoint)},
		{"login", http.MethodGet, fmt.Sprintf("%s%s", a.Env.CredentialsPathPrefix, a.Env.LoginEndpoint)},
		{"register", http.MethodPost, fmt.Sprintf("%s%s", a.Env.CredentialsPathPrefix, a.Env.RegisterEndpoint)},
		{"validateUser", http.MethodGet, fmt.Sprintf("%s%s", a.Env.CredentialsPathPrefix, a.Env.ValidateUserEndpoint)},
	}

	for _, e := range expectedRoutes {
		t.Run(e.name, func(t *testing.T) {
			if !routeExists(routes, e, t) {
				t.Errorf("%s expected route %s %s: registered route failure", e.name, e.method, e.requestURI)
			}

		})
	}
}

func routeExists(routes chi.Routes, expectedRoute route, t *testing.T) bool {
	exists := false
	err := chi.Walk(routes, func(method string, requestURI string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		if strings.EqualFold(method, expectedRoute.method) && strings.EqualFold(requestURI, expectedRoute.requestURI) {
			exists = true
		}
		return nil
	})
	if err != nil {
		t.Error(err)
	}
	return exists
}
