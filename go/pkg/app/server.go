package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

func (a *App) startServer() error {
	ch := make(chan error, 1)
	addr := fmt.Sprintf(":%s", a.Env.ServerPort)
	server := &http.Server{
		Addr:    addr,
		Handler: a.router,
	}
	go func() {
		if a.Env.AppMode == a.Env.AppModeDev {
			if err := server.ListenAndServe(); err != nil {
				ch <- fmt.Errorf("http server failure: %w", err)
			}
		}
		if a.Env.AppMode == a.Env.AppModeProd {
			if err := server.ListenAndServeTLS(a.Env.TLSCert, a.Env.TLSKey); err != nil {
				ch <- fmt.Errorf("https server failure: %w", err)
			}
		}
		close(ch)
	}()
	log.Println("\033[32m", fmt.Sprintf("%s %s://%s%s", a.Env.ServerMessage, a.Env.ServerProtocol, a.Env.ServerHost, addr), "\033[0m")
	select {
	case err := <-ch:
		return err
	case <-a.Ctx.Done():
		const timeout = 2 * time.Second
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()
		server.Shutdown(ctx)
		return nil
	}
}
