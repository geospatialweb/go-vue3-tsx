package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/auth"
)

func Test_App_AuthRequired(t *testing.T) {
	Auth := &auth.Auth{
		JWTDomain: a.Env.JWTDomain,
		JWTExpiry: a.Env.JWTExpiry,
		JWTSecret: a.Env.JWTSecret,
	}
	credentials := &auth.Credentials{
		Username: "foo@bar.com",
		Password: "secretPassword",
		Role:     "user",
	}
	expiredToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiYXVkIjoiZ2Vvc3BhdGlhbHdlYi5jYSIsImV4cCI6MTY3OTk0MzkyNywiaXNzIjoiZ2Vvc3BhdGlhbHdlYi5jYSIsIm5hbWUiOiJqb2huY2FtcGJlbGxAZ2Vvc3BhdGlhbHdlYi5jYSIsInN1YiI6IjEifQ.w8oD07bCbCp7h4vfJU9X4f8Pam4QRrOd-se4Pggices"
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	token, _, _ := Auth.GetJWT(credentials)
	authTests := []struct {
		name       string
		token      string
		authorized bool
	}{
		{"valid token", fmt.Sprintf("Bearer %s", token.Token), true},
		{"valid expired", fmt.Sprintf("Bearer %s", expiredToken), false},
		{"invalid token", fmt.Sprintf("Bearer %s0", token.Token), false},
		{"no token", "", false},
	}

	for _, e := range authTests {
		t.Run(e.name, func(t *testing.T) {
			r, _ := http.NewRequest(http.MethodGet, "/", nil)
			r.Header.Set("Authorization", e.token)
			w := httptest.NewRecorder()
			h := a.authRequired(nextHandler)
			h.ServeHTTP(w, r)
			if w.Code == http.StatusUnauthorized && e.authorized {
				t.Errorf("%s: received status code 401 and should not have", e.name)
			}
			if w.Code != http.StatusUnauthorized && !e.authorized {
				t.Errorf("%s: did not receive status code 401 and should have", e.name)
			}
		})
	}
}
