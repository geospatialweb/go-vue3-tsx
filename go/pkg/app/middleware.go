package app

import (
	"log"
	"net/http"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/auth"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/handler"
)

func (a *App) authRequired(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := &auth.Auth{
			JWTDomain: a.Env.JWTDomain,
			JWTSecret: a.Env.JWTSecret,
		}
		status, err := auth.GetJWTFromHeaderAndVerify(w, r)
		if err != nil {
			handler.WriteJSONResponse(w, err.Error(), status)
			return
		}
		log.Println("\033[36m", "JWT verification success", "\033[0m")
		next.ServeHTTP(w, r)
	})
}
