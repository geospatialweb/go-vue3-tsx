package app

import (
	"context"
	"net/http"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/database/postgres"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/env"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/handler"
	postgresRepo "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/repository/postgres"
)

type App struct {
	Ctx     context.Context
	Env     env.Config
	handler *handler.Handler
	router  http.Handler
}

func (a *App) Start() error {
	p := &postgres.Postgres{
		Ctx: a.Ctx,
		Env: a.Env,
	}
	if err := p.StartPostgres(); err != nil {
		return err
	}
	defer p.Pool.Close()
	a.handler = &handler.Handler{
		DB: &postgresRepo.PostgresRepo{
			Pool: p.Pool,
		},
		Env: a.Env,
	}
	a.router = a.createRouter()
	if err := a.startServer(); err != nil {
		return err
	}
	return nil
}
