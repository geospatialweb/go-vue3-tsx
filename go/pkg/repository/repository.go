package repository

import (
	"context"
	"net/url"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/collection"
	model "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/model/user"
)

type PostgresRepo interface {
	DeleteUser(ctx context.Context, user *model.User) (*model.User, int, error)
	GetFeatures(ctx context.Context, params url.Values) ([]*collection.Feature, int, error)
	GetPassword(ctx context.Context, user *model.User) (*model.User, int, error)
	GetUser(ctx context.Context, user *model.User) (*model.User, int, error)
	InsertUser(ctx context.Context, user *model.User) (*model.User, int, error)
	UpdatePassword(ctx context.Context, user *model.User) (*model.User, int, error)
}
