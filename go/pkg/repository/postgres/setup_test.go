//go:build integration

package postgres

import (
	"context"
	"fmt"
	"os"
	"path"
	"testing"

	"github.com/caarlos0/env/v8"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/joho/godotenv"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/repository"
)

type envConfig struct {
	DockerPort           string `env:"POSTGRES_DOCKER_PORT"`
	DockerRepository     string `env:"POSTGRES_DOCKER_REPOSITORY"`
	DockerTag            string `env:"POSTGRES_DOCKER_TAG"`
	PostgresTestDbName   string `env:"POSTGRES_TEST_DBNAME"`
	PostgresTestDSN      string `env:"POSTGRES_TEST_DSN"`
	PostgresTestHostIp   string `env:"POSTGRES_TEST_HOST_IP"`
	PostgresTestHostPort string `env:"POSTGRES_TEST_HOST_PORT"`
	PostgresTestPassword string `env:"POSTGRES_TEST_PASSWORD"`
	PostgresTestUser     string `env:"POSTGRES_TEST_USER"`
}

type repoTest struct {
	container  *dockertest.Resource
	ctx        context.Context
	db         repository.PostgresRepo
	dbPool     *pgxpool.Pool
	dockerPool *dockertest.Pool
	env        envConfig
}

var r repoTest

func TestMain(m *testing.M) {
	var err error
	r.ctx = context.Background()
	r.env, err = loadEnv()
	if err != nil {
		fmt.Println(err)
	}
	if err = r.createDockerPool(); err != nil {
		fmt.Println(err)
	}
	if err = r.createDockerContainer(); err != nil {
		fmt.Println(err)
		if err := r.purgeDockerContainer(); err != nil {
			fmt.Println(err)
		}
	}
	if err = r.dockerPool.Retry(func() error { return r.setTestPoolConnection() }); err != nil {
		fmt.Println(err)
		if err := r.purgeDockerContainer(); err != nil {
			fmt.Println(err)
		}
	}
	if err = r.setSqlData(); err != nil {
		fmt.Println(err)
	}
	r.setPostgresRepo()
	code := m.Run()
	if err := r.purgeDockerContainer(); err != nil {
		fmt.Println(err)
	}
	os.Exit(code)
}

func loadEnv() (envConfig, error) {
	cfg := envConfig{}
	if err := godotenv.Load("../../../.env"); err != nil {
		return cfg, fmt.Errorf(".env load failure: %w", err)
	}
	opts := env.Options{RequiredIfNoDef: true}
	if err := env.ParseWithOptions(&cfg, opts); err != nil {
		return cfg, fmt.Errorf(".env parse failure: %w", err)
	}
	return cfg, nil
}

func (r *repoTest) createDockerPool() error {
	var err error
	r.dockerPool, err = dockertest.NewPool("")
	if err != nil {
		return fmt.Errorf("docker pool creation failure: %w", err)
	}
	if err := r.dockerPool.Client.Ping(); err != nil {
		return fmt.Errorf("docker pool ping failure: %w", err)
	}
	return nil
}

func (r *repoTest) createDockerContainer() error {
	var err error
	r.container, err = r.dockerPool.RunWithOptions(setRunOptions())
	if err != nil {
		return fmt.Errorf("docker container creation failure: %w", err)
	}
	return nil
}

func setRunOptions() *dockertest.RunOptions {
	dockerPort := docker.Port(r.env.DockerPort)
	options := dockertest.RunOptions{
		Repository: r.env.DockerRepository,
		Tag:        r.env.DockerTag,
		Env: []string{
			fmt.Sprintf("POSTGRES_DBNAME=%s", r.env.PostgresTestDbName),
			fmt.Sprintf("POSTGRES_PASS=%s", r.env.PostgresTestPassword),
			fmt.Sprintf("POSTGRES_USER=%s", r.env.PostgresTestUser),
		},
		ExposedPorts: []string{r.env.DockerPort},
		PortBindings: map[docker.Port][]docker.PortBinding{
			dockerPort: {
				{HostIP: r.env.PostgresTestHostIp, HostPort: r.env.PostgresTestHostPort},
			},
		},
	}
	return &options
}

func (r *repoTest) purgeDockerContainer() error {
	if r.container != nil {
		if err := r.dockerPool.Purge(r.container); err != nil {
			return fmt.Errorf("docker container purge failure: %w", err)
		}
	}
	return nil
}

func (r *repoTest) setTestPoolConnection() error {
	var err error
	r.dbPool, err = pgxpool.New(r.ctx, r.env.PostgresTestDSN)
	if err != nil {
		return fmt.Errorf("test pool creation failure: %w", err)
	}
	return r.pingTestPoolConnection()
}

func (r *repoTest) pingTestPoolConnection() error {
	if err := r.dbPool.Ping(r.ctx); err != nil {
		return fmt.Errorf("test pool ping failure: %w", err)
	}
	return nil
}

func (r *repoTest) setPostgresRepo() {
	r.db = &PostgresRepo{Pool: r.dbPool}
}

func (r *repoTest) setSqlData() error {
	sql, err := readSqlData()
	if err != nil {
		return err
	}
	if err := r.loadSqlData(r.ctx, sql); err != nil {
		return err
	}
	return nil
}

func readSqlData() (string, error) {
	dir := "./testdata"
	fileName := "migration.sql"
	data := path.Join(dir, fileName)
	sql, err := os.ReadFile(data)
	if err != nil {
		return "", fmt.Errorf("sql data read failure: %w", err)
	}
	return string(sql), nil
}

func (r *repoTest) loadSqlData(ctx context.Context, sql string) error {
	_, err := r.dbPool.Exec(ctx, sql)
	if err != nil {
		return fmt.Errorf("sql data load failure: %w", err)
	}
	return nil
}
