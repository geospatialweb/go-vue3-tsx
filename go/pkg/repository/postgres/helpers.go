package postgres

import (
	"fmt"
	"log"
	"runtime"
	"strings"
)

func getMethodName() string {
	const skip = 2
	pc, _, _, _ := runtime.Caller(skip)
	s := strings.Split(runtime.FuncForPC(pc).Name(), ".")
	return s[len(s)-1]
}

func logQueryErrorMessage(err error) error {
	methodName := getMethodName()
	return fmt.Errorf(fmt.Sprintf("%s query failure: %s", methodName, err.Error()))
}

func logQuerySuccessMessage() {
	methodName := getMethodName()
	log.Println("\033[36m", fmt.Sprintf("%s query success", methodName), "\033[0m")
}
