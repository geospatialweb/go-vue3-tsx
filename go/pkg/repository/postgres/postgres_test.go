//go:build integration

package postgres

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/collection"
	model "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/model/user"
)

func Test_Postgres_PingPoolConnection(t *testing.T) {
	t.Run("ping pool connection", func(t *testing.T) {
		if err := r.pingTestPoolConnection(); err != nil {
			t.Error(err)
		}
	})
}

func Test_Postgres_GetFeatures(t *testing.T) {
	params := map[string][]string{"columns": {"name, description, geom"}, "table": {"office"}}
	feature := `{"type": "Feature", "geometry": {"type":"Point","coordinates":[-76.011422,44.384362]}, "properties": {"name": "Frontenac Arch Biosphere Office", "description": "19 Reynolds Road, Lansdowne, ON. Open Monday to Friday 8:30am - 4:30pm"}}`
	expectedFeatures := []*collection.Feature{{Feature: feature}}
	expectedStatusCode := http.StatusOK

	t.Run("get features", func(t *testing.T) {
		features, status, err := r.db.GetFeatures(r.ctx, params)
		if err != nil {
			t.Error(err)
		}
		if !reflect.DeepEqual(features, expectedFeatures) {
			t.Error("features and expectedFeatures equality match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}

func Test_Postgres_UserQuery(t *testing.T) {
	expectedUsername := "foo@bar.com"
	expectedStatusCode := http.StatusOK

	t.Run("insert user", func(t *testing.T) {
		expectedStatusCode := http.StatusCreated
		user := &model.User{
			Password: "$2a$12$2nUY8g1EhcakgXZSp0nWdOCn.6mvxpUHxx9Y8WjUAB7.PBY8VVCt2",
			Username: "foo@bar.com",
		}
		u, status, err := r.db.InsertUser(r.ctx, user)
		if err != nil {
			t.Fatal(err)
		}
		if u.Username != expectedUsername {
			t.Error("username and expectedUsername match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("get user", func(t *testing.T) {
		user := &model.User{
			Username: "foo@bar.com",
		}
		u, status, err := r.db.GetUser(r.ctx, user)
		if err != nil {
			t.Error(err)
		}
		if u.Username != expectedUsername {
			t.Error("username and expectedUsername match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("update password", func(t *testing.T) {
		expectedPassword := "2a$12$OlOdf9fWqVQskeph1yJkOesvDffclPIF1ya2Dpm1mxdGk8QpdscAK"
		user := &model.User{
			Username: "foo@bar.com",
			Password: "2a$12$OlOdf9fWqVQskeph1yJkOesvDffclPIF1ya2Dpm1mxdGk8QpdscAK",
		}
		u, status, err := r.db.UpdatePassword(r.ctx, user)
		if err != nil {
			t.Error(err)
		}
		if u.Password != expectedPassword {
			t.Error("password and expectedPassword match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("delete user", func(t *testing.T) {
		user := &model.User{
			Username: "foo@bar.com",
		}
		u, status, err := r.db.DeleteUser(r.ctx, user)
		if err != nil {
			t.Error(err)
		}
		if u.Username != expectedUsername {
			t.Error("username and expectedUsername match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}
