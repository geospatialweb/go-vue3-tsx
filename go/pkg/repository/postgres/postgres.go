package postgres

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/collection"
	model "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/model/user"
)

type PostgresRepo struct {
	Pool *pgxpool.Pool
}

const timeout = 2 * time.Second

func (p *PostgresRepo) GetFeatures(ctx context.Context, params url.Values) ([]*collection.Feature, int, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	c := &collection.Collection{}
	keys := []string{"columns", "table"}
	format := `SELECT ST_AsGeoJSON(feature.*) AS feature
			   FROM (SELECT %s FROM %s) AS feature`
	sql := fmt.Sprintf(format, params.Get(keys[0]), params.Get(keys[1]))
	if err := pgxscan.Select(ctx, p.Pool, &c.Features, sql); err != nil {
		return c.Features, http.StatusBadRequest, logQueryErrorMessage(err)
	}
	logQuerySuccessMessage()
	return c.Features, http.StatusOK, nil
}

func (p *PostgresRepo) DeleteUser(ctx context.Context, user *model.User) (*model.User, int, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	u := &model.User{}
	args := pgx.NamedArgs{
		"username": user.Username,
	}
	sql := `DELETE
			FROM users
			WHERE username=@username
			RETURNING username`
	if err := p.Pool.QueryRow(ctx, sql, args).Scan(&u.Username); err != nil {
		return u, http.StatusBadRequest, logQueryErrorMessage(err)
	}
	logQuerySuccessMessage()
	return u, http.StatusOK, nil
}

func (p *PostgresRepo) GetUser(ctx context.Context, user *model.User) (*model.User, int, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	u := &model.User{}
	args := pgx.NamedArgs{
		"username": user.Username,
	}
	sql := `SELECT username, role
			FROM users
			WHERE username=@username`
	if err := p.Pool.QueryRow(ctx, sql, args).Scan(&u.Username, &u.Role); err != nil {
		return u, http.StatusBadRequest, logQueryErrorMessage(err)
	}
	logQuerySuccessMessage()
	return u, http.StatusOK, nil
}

func (p *PostgresRepo) InsertUser(ctx context.Context, user *model.User) (*model.User, int, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	u := &model.User{}
	args := pgx.NamedArgs{
		"username": user.Username,
		"password": user.Password,
		"role":     user.Role,
	}
	sql := `INSERT INTO users (username, password, role)
			VALUES (@username, @password, @role)
			RETURNING username`
	if err := p.Pool.QueryRow(ctx, sql, args).Scan(&u.Username); err != nil {
		return u, http.StatusBadRequest, logQueryErrorMessage(err)
	}
	logQuerySuccessMessage()
	return u, http.StatusCreated, nil
}

func (p *PostgresRepo) GetPassword(ctx context.Context, user *model.User) (*model.User, int, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	u := &model.User{}
	args := pgx.NamedArgs{
		"username": user.Username,
	}
	sql := `SELECT password
			FROM users
			WHERE username=@username`
	if err := p.Pool.QueryRow(ctx, sql, args).Scan(&u.Password); err != nil {
		return u, http.StatusBadRequest, logQueryErrorMessage(err)
	}
	logQuerySuccessMessage()
	return u, http.StatusOK, nil
}

func (p *PostgresRepo) UpdatePassword(ctx context.Context, user *model.User) (*model.User, int, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	u := &model.User{}
	args := pgx.NamedArgs{
		"username": user.Username,
		"password": user.Password,
	}
	sql := `UPDATE users
			SET password=@password
			WHERE username=@username
			RETURNING password, username`
	if err := p.Pool.QueryRow(ctx, sql, args).Scan(&u.Password, &u.Username); err != nil {
		return u, http.StatusBadRequest, logQueryErrorMessage(err)
	}
	logQuerySuccessMessage()
	return u, http.StatusOK, nil
}
