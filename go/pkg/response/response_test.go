package response

import (
	"net/http"
	"net/http/httptest"
	"testing"

	geoJSON "github.com/paulmach/go.geojson"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/collection"
)

func Test_Response_WriteJSONResponse(t *testing.T) {
	var res Response
	feature := `{"type": "Feature", "geometry": {"type": "Point", "coordinates": [-76.011422,44.384362]},"properties": {"name": "Frontenac Arch Biosphere Office", "description": "19 Reynolds Road, Lansdowne, ON. Open Monday to Friday 8:30am - 4:30pm"}}`
	c := collection.Collection{Features: []*collection.Feature{{Feature: feature}}}
	features, _, _ := c.CreateGeoJSONFeatures()
	res.Payload = geoJSON.FeatureCollection{Type: "FeatureCollection", Features: features}
	res.Status = http.StatusOK
	res.Writer = httptest.NewRecorder()

	t.Run("write json response", func(t *testing.T) {
		status, _ := res.WriteJSONResponse()
		if status != res.Status {
			t.Error("status code and res.Status match failure")
		}
	})
}

func Test_Response_ValidateJSON(t *testing.T) {
	t.Run("validate good json", func(t *testing.T) {
		var res Response
		json := `{"username": "foobar.com", "password": "foobar"}`
		expectedStatusCode := http.StatusOK
		status, _ := res.validateJSON([]byte(json))
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("validate bad json", func(t *testing.T) {
		var res Response
		json := "{'username': 'foobar.com', 'password': 'foobar'}"
		expectedStatusCode := http.StatusInternalServerError
		status, _ := res.validateJSON([]byte(json))
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}
