package response

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/validation"
)

type Response struct {
	Payload interface{}
	Status  int
	Writer  http.ResponseWriter
}

func (res *Response) WriteJSONResponse() (int, error) {
	json, status, err := res.marshalJSON()
	if err != nil {
		return status, err
	}
	status, err = res.validateJSON(json)
	if err != nil {
		return status, err
	}
	status, err = res.writeJSON(json)
	if err != nil {
		return status, err
	}
	return status, nil
}

func (res *Response) marshalJSON() ([]byte, int, error) {
	json, err := json.Marshal(res.Payload)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("json payload encode failure: %w", err)
	}
	return json, res.Status, nil
}

func (res *Response) validateJSON(json []byte) (int, error) {
	status, err := validation.ValidateJSON(json)
	if err != nil {
		return status, err
	}
	return status, nil
}

func (res *Response) writeJSON(json []byte) (int, error) {
	res.Writer.Header().Set("Content-Type", "application/json")
	res.Writer.WriteHeader(res.Status)
	_, err := res.Writer.Write(json)
	if err != nil {
		return http.StatusInternalServerError, fmt.Errorf("response write failure: %w", err)
	}
	return res.Status, nil
}
