package auth

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"

	model "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/model/user"
)

type Auth struct {
	JWTDomain string
	JWTExpiry string
	JWTSecret string
}

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type claims struct {
	jwt.RegisteredClaims
}

type jwtToken struct {
	Expiry int64  `json:"jwtExpiry"`
	Token  string `json:"jwtToken"`
}

func CompareHashAndPassword(c *Credentials, u *model.User) (int, error) {
	if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(c.Password)); err != nil {
		return http.StatusUnauthorized, fmt.Errorf("hash/password comparison failure: %w", err)
	}
	return 0, nil
}

func GenerateHashFromPassword(password string) (string, int, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return "", http.StatusInternalServerError, fmt.Errorf("hash password generation failure: %w", err)
	}
	return string(hash), 0, nil
}

func (a *Auth) GetJWT(c *Credentials) (jwtToken, int, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	expiry := a.setJWTClaims(c, token)
	signedJWT, err := token.SignedString([]byte(a.JWTSecret))
	if err != nil {
		return jwtToken{}, http.StatusInternalServerError, fmt.Errorf("jwt signature failure: %w", err)
	}
	jwt := jwtToken{
		Token:  signedJWT,
		Expiry: expiry,
	}
	return jwt, http.StatusOK, nil
}

func (a *Auth) setJWTClaims(c *Credentials, token *jwt.Token) int64 {
	jwtClaims := token.Claims.(jwt.MapClaims)
	jwtClaims["aud"] = c.Role
	jwtClaims["exp"] = time.Now().Add(a.setJWTExpiry()).Unix()
	jwtClaims["iat"] = time.Now().Unix()
	jwtClaims["iss"] = a.JWTDomain
	jwtClaims["sub"] = c.Username
	return jwtClaims["exp"].(int64)
}

func (a *Auth) setJWTExpiry() time.Duration {
	jwtExpiry, _ := strconv.Atoi(a.JWTExpiry)
	return time.Duration(jwtExpiry) * time.Minute
}

func (a *Auth) GetJWTFromHeaderAndVerify(w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Add("Vary", "Authorization")
	authHeader := r.Header.Get("Authorization")
	token, status, err := validateAuthHeader(authHeader)
	if err != nil {
		return status, err
	}
	status, err = a.verifyJWT(token)
	return status, err
}

func validateAuthHeader(authHeader string) (string, int, error) {
	if authHeader == "" {
		return "", http.StatusUnauthorized, errors.New("auth header missing")
	}
	authHeaderElements := strings.Split(authHeader, " ")
	if len(authHeaderElements) != 2 {
		return "", http.StatusUnauthorized, errors.New("auth header invalid")
	}
	if authHeaderElements[0] != "Bearer" {
		return "", http.StatusUnauthorized, errors.New("no Bearer in auth header")
	}
	return authHeaderElements[1], 0, nil
}

func (a *Auth) verifyJWT(token string) (int, error) {
	claims := &claims{}
	status, err := a.parseJWTWithClaims(token, claims)
	if err != nil {
		return status, err
	}
	status, err = a.validateClaimsIssuer(claims)
	if err != nil {
		return status, err
	}
	return http.StatusOK, nil
}

func (a *Auth) parseJWTWithClaims(token string, claims *claims) (int, error) {
	_, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("jwt unexpected signing method:\n%v", token.Header["alg"])
		}
		return []byte(a.JWTSecret), nil
	})
	if err != nil {
		if strings.HasPrefix(err.Error(), "token is expired by") {
			return http.StatusUnauthorized, fmt.Errorf("jwt expired: %w", err)
		}
		return http.StatusUnauthorized, fmt.Errorf("parse jwt with claims failure: %w", err)
	}
	return 0, nil
}

func (a *Auth) validateClaimsIssuer(claims *claims) (int, error) {
	if claims.Issuer != a.JWTDomain {
		return http.StatusUnauthorized, errors.New("claims issuer validation failure")
	}
	return 0, nil
}
