package auth

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	model "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/model/user"
)

func Test_Auth_AuthToken(t *testing.T) {
	expiredToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiYXVkIjoiZ2Vvc3BhdGlhbHdlYi5jYSIsImV4cCI6MTY3OTk0MzkyNywiaXNzIjoiZ2Vvc3BhdGlhbHdlYi5jYSIsIm5hbWUiOiJqb2huY2FtcGJlbGxAZ2Vvc3BhdGlhbHdlYi5jYSIsInN1YiI6IjEifQ.w8oD07bCbCp7h4vfJU9X4f8Pam4QRrOd-se4Pggices"
	credentials := &Credentials{
		Username: "foo@bar.com",
		Password: "secretPassword",
		Role:     "user",
	}
	token, _, _ := a.GetJWT(credentials)
	tokenTests := []struct {
		name               string
		token              string
		expectedError      bool
		expectedStatusCode int
	}{
		{"valid", fmt.Sprintf("Bearer %s", token.Token), false, http.StatusOK},
		{"valid expired", fmt.Sprintf("Bearer %s", expiredToken), true, http.StatusUnauthorized},
		{"invalid token", fmt.Sprintf("Bearer %s0", token.Token), true, http.StatusUnauthorized},
		{"no bearer", fmt.Sprintf("Bear %s", token.Token), true, http.StatusUnauthorized},
		{"no token", "Bearer", true, http.StatusUnauthorized},
		{"no header", "", true, http.StatusUnauthorized},
	}

	for _, e := range tokenTests {
		t.Run(e.name, func(t *testing.T) {
			r, _ := http.NewRequest(http.MethodGet, "/", nil)
			r.Header.Set("Authorization", e.token)
			w := httptest.NewRecorder()
			status, err := a.GetJWTFromHeaderAndVerify(w, r)
			if err != nil && !e.expectedError {
				t.Errorf("%s: did not expect error, but received error: %s", e.name, err.Error())
			}
			if err == nil && e.expectedError {
				t.Errorf("%s: expected error, but did not receive error", e.name)
			}
			if status != e.expectedStatusCode {
				t.Error("status code and expectedStatusCode match failure")
			}
		})
	}
}

func Test_Auth_Hash_Password(t *testing.T) {
	expectedHash := []byte(e.PostgresAdminPassword)
	password := "secretPassword"
	hash, _, _ := GenerateHashFromPassword(password)
	if reflect.DeepEqual(hash, expectedHash) {
		t.Error("hash and expectedStatusCode match failure")
	}
	c := &Credentials{
		Password: password,
	}
	u := &model.User{
		Password: string(hash),
	}
	_, err := CompareHashAndPassword(c, u)
	if err != nil {
		t.Error(err)
	}
}
