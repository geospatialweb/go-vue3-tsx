package auth

import (
	"os"
	"testing"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/env"
)

var a *Auth
var e env.Config

func TestMain(m *testing.M) {
	e, _ = env.LoadEnv("../../.env")
	a = &Auth{
		JWTDomain: e.JWTDomain,
		JWTExpiry: e.JWTExpiry,
		JWTSecret: e.JWTSecret,
	}
	os.Exit(m.Run())
}
