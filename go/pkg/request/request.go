package request

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/golang/gddo/httputil/header"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/validation"
)

type Request struct {
	RequestURI string
	UrlQuery   string
}

func (req *Request) GetUrlQueryParams() (url.Values, int, error) {
	status, err := req.validateRequestURI()
	if err != nil {
		return nil, status, err
	}
	params, err := url.ParseQuery(req.UrlQuery)
	if err != nil {
		return nil, http.StatusBadRequest, fmt.Errorf("url query parse failure: %w", err)
	}
	return params, status, nil
}

func (req *Request) validateRequestURI() (int, error) {
	status, err := validation.ValidateRequestURI(req.RequestURI)
	if err != nil {
		return status, err
	}
	return status, nil
}

/*
 * How to Parse a JSON Request Body in Go
 * Published by: Alex Edwards
 * Published on: October 21, 2019
 * URL: https://www.alexedwards.net/blog/how-to-properly-parse-a-json-request-body
 */
func (req *Request) DecodeJSONBody(w http.ResponseWriter, r *http.Request, dst interface{}) (int, error) {
	if r.Header.Get("Content-Type") != "" {
		value, _ := header.ParseValueAndParams(r.Header, "Content-Type")
		if value != "application/json" {
			return http.StatusUnsupportedMediaType, fmt.Errorf("Content-Type header is not application/json")
		}
	}
	const maxBytes int64 = 1024 * 1024
	r.Body = http.MaxBytesReader(w, r.Body, maxBytes)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	err := dec.Decode(&dst)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		switch {
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("request body contains badly-formed JSON (at position %d):", syntaxError.Offset) + " %w"
			return http.StatusBadRequest, fmt.Errorf(msg, err)

		case errors.Is(err, io.ErrUnexpectedEOF):
			msg := "request body contains badly-formed JSON: %w"
			return http.StatusBadRequest, fmt.Errorf(msg, err)

		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("request body contains an invalid value for the %q field (at position %d):", unmarshalTypeError.Field, unmarshalTypeError.Offset) + " %w"
			return http.StatusBadRequest, fmt.Errorf(msg, err)

		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			msg := fmt.Sprintf("request body contains unknown field %s:", fieldName) + " %w"
			return http.StatusBadRequest, fmt.Errorf(msg, err)

		case errors.Is(err, io.EOF):
			msg := "request body must not be empty: %w"
			return http.StatusBadRequest, fmt.Errorf(msg, err)

		case err.Error() == "http: request body too large":
			msg := "request body must not be larger than 1MB: %w"
			return http.StatusRequestEntityTooLarge, fmt.Errorf(msg, err)

		default:
			return http.StatusInternalServerError, err
		}
	}
	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		msg := "request body must only contain a single JSON object: %w"
		return http.StatusBadRequest, fmt.Errorf(msg, err)
	}
	return http.StatusOK, nil
}
