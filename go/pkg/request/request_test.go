package request

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"reflect"
	"strings"
	"testing"
)

func Test_Request_GetUrlQueryParams(t *testing.T) {
	var req Request
	req.RequestURI = "/api/geojson?columns=name,description,geom&table=office"
	req.UrlQuery = "columns=name,description,geom&table=office"
	expectedStatusCode := http.StatusOK
	expectedParams := map[string][]string{
		"columns": {"name,description,geom"},
		"table":   {"office"},
	}

	t.Run("url query params", func(t *testing.T) {
		params, status, _ := req.GetUrlQueryParams()
		if !reflect.DeepEqual(params, url.Values(expectedParams)) {
			t.Error("params and expectedParams equality match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}

func Test_Request_ValidateBadRequestURI(t *testing.T) {
	var req Request
	req.RequestURI = `\api\geojson?columns=name,description,geom&table=office`
	expectedStatusCode := http.StatusBadRequest

	t.Run("bad request URI", func(t *testing.T) {
		_, status, _ := req.GetUrlQueryParams()
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}

type credentials struct {
	Username string
	Password string
	Role string
}

func Test_Request_DecodeJSONBody(t *testing.T) {
	json, _ := os.ReadFile("./testdata/large-file.json")
	largeJSON := string(json) // in excess of 1MB limit
	expectedStatusCodes := []struct {
		name               string
		method             string
		target             string
		headerKey          string
		headerValue        string
		body               string
		expectedStatusCode int
	}{
		{"badly-formed JSON", http.MethodPost, "/", "Content-Type", "application/json", "{username: foobar.com, password: foobar}", http.StatusBadRequest},
		{"empty body", http.MethodPost, "/", "Content-Type", "application/json", "", http.StatusBadRequest},
		{"incorrect fields", http.MethodPost, "/", "Content-Type", "application/json", `{"gender: "male", "username": "foobar.com", "password": "foobar"}`, http.StatusBadRequest},
		{"incorrect field type", http.MethodPut, "/", "Content-Type", "application/json", `{"gender: "male", "username": "foobar.com", "password": "foobar"}`, http.StatusBadRequest},
		{"incorrect header value", http.MethodPost, "/", "Content-Type", "application/xml", "{}", http.StatusUnsupportedMediaType},
		{"too large JSON", http.MethodPut, "/", "Content-Type", "application/json", largeJSON, http.StatusRequestEntityTooLarge},
		{"too many fields", http.MethodPut, "/", "Content-Type", "application/json", `{"gender: "male", "username":
			"foobar.com", "password": "foobar"}`, http.StatusBadRequest},
		{"too many objects", http.MethodPut, "/", "Content-Type", "application/json", `{"username": "foobar.com", "password": "foobar"},{}`, http.StatusBadRequest},
		{"unexpected EOF", http.MethodPost, "/", "Content-Type", "application/json", `{"username": "foobar.com", "password": "foobar"`, http.StatusBadRequest},
		{"unknown field", http.MethodPut, "/", "Content-Type", "application/json", `{"gender": "male"}`, http.StatusBadRequest},
		{"valid JSON", http.MethodPut, "/", "Content-Type", "application/json", `{"username": "foobar.com", "password": "foobar"}`, http.StatusOK},
	}

	for _, e := range expectedStatusCodes {
		t.Run(e.name, func(t *testing.T) {
			var req Request
			c := &credentials{}
			r := httptest.NewRequest(e.method, e.target, strings.NewReader(e.body))
			r.Header.Set(e.headerKey, e.headerValue)
			w := httptest.NewRecorder()
			status, _ := req.DecodeJSONBody(w, r, c)
			if status != e.expectedStatusCode {
				msg := "for test '%s': expected status code %d, but received status code %d"
				t.Errorf(msg, e.name, e.expectedStatusCode, status)
			}
		})
	}
}
