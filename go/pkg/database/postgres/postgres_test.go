package postgres

import (
	"strings"
	"testing"
)

func Test_Postgres_StartPostgres(t *testing.T) {
	t.Run("pool connection success", func(t *testing.T) {
		if err := p.StartPostgres(); err != nil {
			t.Error(err)
		}
	})

	t.Run("pool connection failure", func(t *testing.T) {
		p.Env.PostgresDSN = "postgres://admin:secret0@host.docker.internal:5432/postgres"
		if err := p.StartPostgres(); err != nil {
			if !strings.HasPrefix(err.Error(), "pool ping failure") {
				t.Error(err)
			}
		}
	})
}
