package postgres

import (
	"context"
	"os"
	"testing"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/env"
)

var p *Postgres

func TestMain(m *testing.M) {
	env, _ := env.LoadEnv("../../../.env")
	ctx := context.Background()
	p = &Postgres{
		Ctx: ctx,
		Env: env,
	}
	os.Exit(m.Run())
}
