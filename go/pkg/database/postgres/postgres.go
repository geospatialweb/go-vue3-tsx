package postgres

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/env"
)

type Postgres struct {
	Ctx  context.Context
	Env  env.Config
	Pool *pgxpool.Pool
}

func (p *Postgres) StartPostgres() error {
	if err := p.setPoolConnection(); err != nil {
		return err
	}
	if err := p.pingPoolConnection(); err != nil {
		return err
	}
	return nil
}

func (p *Postgres) setPoolConnection() error {
	var err error
	p.Pool, err = pgxpool.New(p.Ctx, p.Env.PostgresDSN)
	if err != nil {
		return fmt.Errorf("pool creation failure: %w", err)
	}
	log.Println("\033[36m", "Pool creation success", "\033[0m")
	return nil
}

func (p *Postgres) pingPoolConnection() error {
	if err := p.Pool.Ping(p.Ctx); err != nil {
		return fmt.Errorf("pool ping failure: %w", err)
	}
	log.Println("\033[36m", "Pool ping success", "\033[0m")
	return nil
}
