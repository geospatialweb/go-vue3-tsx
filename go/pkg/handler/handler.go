package handler

import (
	"net/http"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/auth"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/collection"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/env"
	model "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/model/user"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/repository"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/request"
)

type Handler struct {
	DB  repository.PostgresRepo
	Env env.Config
}

type MapboxAccessToken struct {
	Token string `json:"token"`
}

func (h *Handler) GetGeoJSONFeatureCollection(w http.ResponseWriter, r *http.Request) {
	req := &request.Request{
		RequestURI: r.RequestURI,
		UrlQuery:   r.URL.RawQuery,
	}
	params, status, err := req.GetUrlQueryParams()
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	features, status, err := h.DB.GetFeatures(r.Context(), params)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	c := &collection.Collection{
		Features: features,
	}
	fc, status, err := c.CreateGeoJSONFeatureCollection()
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	WriteJSONResponse(w, fc, status)
}

func (h *Handler) GetMapboxAccessToken(w http.ResponseWriter, r *http.Request) {
	mapboxAccessToken := MapboxAccessToken{Token: h.Env.MapboxAccessToken}
	WriteJSONResponse(w, mapboxAccessToken, http.StatusOK)
}

func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	req := &request.Request{
		RequestURI: r.RequestURI,
		UrlQuery:   r.URL.RawQuery,
	}
	params, status, err := req.GetUrlQueryParams()
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	username := params.Get("username")
	status, err = validateUsername(username)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	user := &model.User{
		Username: username,
	}
	u, status, err := h.DB.GetUser(r.Context(), user)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	WriteJSONResponse(w, u, status)
}

func (h *Handler) LoginUser(w http.ResponseWriter, r *http.Request) {
	req := &request.Request{
		RequestURI: r.RequestURI,
		UrlQuery:   r.URL.RawQuery,
	}
	params, status, err := req.GetUrlQueryParams()
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	username := params.Get("username")
	status, err = validateUsername(username)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	user := &model.User{
		Username: username,
	}
	u, status, err := h.DB.GetPassword(r.Context(), user)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	c := &auth.Credentials{
		Username: username,
		Password: params.Get("password"),
		Role:     params.Get("role"),
	}
	status, err = auth.CompareHashAndPassword(c, u)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	a := auth.Auth{
		JWTDomain: h.Env.JWTDomain,
		JWTExpiry: h.Env.JWTExpiry,
		JWTSecret: h.Env.JWTSecret,
	}
	jwt, status, err := a.GetJWT(c)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	WriteJSONResponse(w, jwt, status)
}

func (h *Handler) RegisterUser(w http.ResponseWriter, r *http.Request) {
	req := &request.Request{
		RequestURI: r.RequestURI,
	}
	c := &auth.Credentials{}
	status, err := req.DecodeJSONBody(w, r, c)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	status, err = validateUsername(c.Username)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	hash, status, err := auth.GenerateHashFromPassword(c.Password)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	user := &model.User{
		Username: c.Username,
		Password: hash,
		Role:     c.Role,
	}
	u, status, err := h.DB.InsertUser(r.Context(), user)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	WriteJSONResponse(w, u, status)
}

func (h *Handler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	req := &request.Request{
		RequestURI: r.RequestURI,
		UrlQuery:   r.URL.RawQuery,
	}
	params, status, err := req.GetUrlQueryParams()
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	username := params.Get("username")
	status, err = validateUsername(username)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	user := &model.User{
		Username: username,
	}
	u, status, err := h.DB.DeleteUser(r.Context(), user)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	WriteJSONResponse(w, u, status)
}

func (h *Handler) UpdatePassword(w http.ResponseWriter, r *http.Request) {
	req := &request.Request{
		RequestURI: r.RequestURI,
	}
	c := &auth.Credentials{}
	status, err := req.DecodeJSONBody(w, r, c)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	username := c.Username
	status, err = validateUsername(username)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	hash, status, err := auth.GenerateHashFromPassword(c.Password)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	user := &model.User{
		Username: username,
		Password: hash,
	}
	u, status, err := h.DB.UpdatePassword(r.Context(), user)
	if err != nil {
		WriteJSONResponse(w, err.Error(), status)
		return
	}
	u = &model.User{
		Username: u.Username,
	}
	WriteJSONResponse(w, u, status)
}
