package handler

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_Handler_Handlers(t *testing.T) {
	deleteUserUrlPath := fmt.Sprintf("%s%s", h.Env.ApiPathPrefix, h.Env.DeleteUserEndpoint)
	geoJSONUrlPath := fmt.Sprintf("%s%s", h.Env.ApiPathPrefix, h.Env.GeoJSONEndpoint)
	mapboxAccessTokenUrlPath := fmt.Sprintf("%s%s", h.Env.ApiPathPrefix, h.Env.MapboxAccessTokenEndpoint)
	updatePasswordUrlPath := fmt.Sprintf("%s%s", h.Env.ApiPathPrefix, h.Env.UpdatePasswordEndpoint)
	getUserUrlPath := fmt.Sprintf("%s%s", h.Env.CredentialsPathPrefix, h.Env.GetUserEndpoint)
	loginUrlPath := fmt.Sprintf("%s%s", h.Env.CredentialsPathPrefix, h.Env.LoginEndpoint)
	registerUrlPath := fmt.Sprintf("%s%s", h.Env.CredentialsPathPrefix, h.Env.RegisterEndpoint)
	expectedStatusCodes := []struct {
		name               string
		handler            func(w http.ResponseWriter, r *http.Request)
		method             string
		target             string
		body               string
		expectedStatusCode int
	}{
		{"geoJSON", h.GetGeoJSONFeatureCollection, http.MethodGet, fmt.Sprintf("%s%s", geoJSONUrlPath, "?columns=name,description,geom&table=office"), "", http.StatusOK},
		{"geoJSON bad url query", h.GetGeoJSONFeatureCollection, http.MethodGet, fmt.Sprintf("%s%s", geoJSONUrlPath, "?columns=name,description,geomtable=office"), "", http.StatusBadRequest},
		{"mapboxAccessToken", h.GetMapboxAccessToken, http.MethodGet, mapboxAccessTokenUrlPath, "", http.StatusOK},
		{"register", h.RegisterUser, http.MethodPost, registerUrlPath, `{"username": "foo@bar.com", "password": "foobar", "role": "user"}`, http.StatusCreated},
		{"registerUser invalid username format", h.RegisterUser, http.MethodPost, registerUrlPath, `{"username": "foobar.com", "password": "foobar", "role": "user"}`, http.StatusBadRequest},
		{"registerUser invalid json body format", h.RegisterUser, http.MethodPost, registerUrlPath, "{'username': 'foo@bar.com', 'password': 'foobar', 'role': 'user'}", http.StatusBadRequest},
		{"registerUser empty json body", h.RegisterUser, http.MethodPost, registerUrlPath, "{}", http.StatusBadRequest},
		{"registerUser no json body", h.RegisterUser, http.MethodPost, registerUrlPath, "", http.StatusBadRequest},
		{"login", h.LoginUser, http.MethodGet, fmt.Sprintf("%s%s", loginUrlPath, "?username=foo@bar.com&password=foobar"), "", http.StatusOK},
		{"loginUser invalid username", h.LoginUser, http.MethodGet, fmt.Sprintf("%s%s", loginUrlPath, "?username=foo@bar.co&password=foobar"), "", http.StatusBadRequest},
		{"loginUser invalid username format", h.LoginUser, http.MethodGet, fmt.Sprintf("%s%s", loginUrlPath, "?username=foobar.com&password=foobar"), "", http.StatusBadRequest},
		{"loginUser invalid password", h.LoginUser, http.MethodGet, fmt.Sprintf("%s%s", loginUrlPath, "?username=foo@bar.com&password=fubar"), "", http.StatusUnauthorized},
		{"getUser", h.GetUser, http.MethodGet, fmt.Sprintf("%s%s", getUserUrlPath, "?username=foo@bar.com"), "", http.StatusOK},
		{"getUser invalid username", h.GetUser, http.MethodGet, fmt.Sprintf("%s%s", getUserUrlPath, "?username=foo@bar.co"), "", http.StatusBadRequest},
		{"getUser invalid username format", h.GetUser, http.MethodGet, fmt.Sprintf("%s%s", getUserUrlPath, "?username=foobar.com"), "", http.StatusBadRequest},
		{"updatePassword", h.UpdatePassword, http.MethodPatch, updatePasswordUrlPath, `{"username": "foo@bar.com", "password": "foobar"}`, http.StatusOK},
		{"updatePassword invalid username format", h.UpdatePassword, http.MethodPatch, updatePasswordUrlPath, `{"username": "foobar@bar.com", "password": "foobar"}`, http.StatusBadRequest},
		{"deleteUser", h.DeleteUser, http.MethodDelete, fmt.Sprintf("%s%s", deleteUserUrlPath, "?username=foo@bar.com"), "", http.StatusOK},
		{"deleteUser invalid username format", h.DeleteUser, http.MethodDelete, deleteUserUrlPath, `{"username": "foobar.com", "password": "foobar"}`, http.StatusBadRequest},
	}

	for _, e := range expectedStatusCodes {
		t.Run(e.name, func(t *testing.T) {
			var r *http.Request
			if e.body == "" {
				r = httptest.NewRequest(e.method, e.target, nil)
			}
			if e.body != "" {
				r = httptest.NewRequest(e.method, e.target, strings.NewReader(e.body))
			}
			w := httptest.NewRecorder()
			h := http.HandlerFunc(e.handler)
			h.ServeHTTP(w, r)
			if w.Code != e.expectedStatusCode {
				msg := "for test '%s' %s %s: expected status code %d, but received status code %d"
				t.Errorf(msg, e.name, e.method, e.target, e.expectedStatusCode, w.Code)
			}
		})
	}
}
