package handler

import (
	"log"
	"net/http"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/response"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/validation"
)

func validateUsername(username string) (int, error) {
	status, err := validation.ValidateEmailFormat(username)
	if err != nil {
		return status, err
	}
	return status, nil
}

func WriteJSONResponse(w http.ResponseWriter, payload interface{}, status int) {
	res := &response.Response{
		Writer:  w,
		Payload: payload,
		Status:  status,
	}
	status, err := res.WriteJSONResponse()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), status)
	}
}
