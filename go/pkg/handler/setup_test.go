package handler

import (
	"context"
	"os"
	"testing"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/database/postgres"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/env"
	postgresRepo "gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/repository/postgres"
)

var h *Handler

func TestMain(m *testing.M) {
	env, _ := env.LoadEnv("../../.env")
	ctx := context.Background()
	p := &postgres.Postgres{
		Ctx: ctx,
		Env: env,
	}
	p.StartPostgres()
	h = &Handler{
		DB: &postgresRepo.PostgresRepo{
			Pool: p.Pool,
		},
		Env: env,
	}
	os.Exit(m.Run())
}
