package collection

import (
	"fmt"
	"net/http"

	geoJSON "github.com/paulmach/go.geojson"
)

type Collection struct {
	Features []*Feature
}

type Feature struct {
	Feature string
}

func (c *Collection) CreateGeoJSONFeatureCollection() (*geoJSON.FeatureCollection, int, error) {
	var err error
	var status int
	fc := geoJSON.NewFeatureCollection()
	fc.Features, status, err = c.CreateGeoJSONFeatures()
	if err != nil {
		return fc, status, err
	}
	return fc, status, nil
}

func (c *Collection) CreateGeoJSONFeatures() ([]*geoJSON.Feature, int, error) {
	var features []*geoJSON.Feature
	for _, f := range c.Features {
		feature, err := geoJSON.UnmarshalFeature([]byte(f.Feature))
		if err != nil {
			msg := "geoJSON Feature decode failure: %w"
			return features, http.StatusBadRequest, fmt.Errorf(msg, err)
		}
		features = append(features, feature)
	}
	return features, http.StatusOK, nil
}
