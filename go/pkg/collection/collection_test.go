package collection

import (
	"net/http"
	"reflect"
	"testing"

	geoJSON "github.com/paulmach/go.geojson"
)

func Test_Collection_CreateGeoJSON_Features(t *testing.T) {
	feature := `{"type": "Feature", "geometry": {"type": "Point", "coordinates": [-76.011422,44.384362]},"properties": {"name": "Frontenac Arch Biosphere Office", "description": "19 Reynolds Road, Lansdowne, ON. Open Monday to Friday 8:30am - 4:30pm"}}`
	c := Collection{Features: []*Feature{{Feature: feature}, {Feature: feature}}}
	expectedFeature, _ := geoJSON.UnmarshalFeature([]byte(feature))
	expectedFeatures := []*geoJSON.Feature{expectedFeature, expectedFeature}
	expectedFC := &geoJSON.FeatureCollection{
		Type:     "FeatureCollection",
		Features: expectedFeatures,
	}
	expectedStatusCode := http.StatusOK

	t.Run("create geoJSON features", func(t *testing.T) {
		features, status, _ := c.CreateGeoJSONFeatures()
		if !reflect.DeepEqual(features, expectedFeatures) {
			t.Error("feature and expectedFeature equality match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("create geoJSON feature collection", func(t *testing.T) {
		fc, status, _ := c.CreateGeoJSONFeatureCollection()
		if !reflect.DeepEqual(fc, expectedFC) {
			t.Error("fc and expectedFC equality match failure")
		}
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}
