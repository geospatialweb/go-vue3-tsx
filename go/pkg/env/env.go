package env

import (
	"fmt"
	"log"

	"github.com/caarlos0/env/v8"
	"github.com/joho/godotenv"
)

type Config struct {
	ApiPathPrefix             string `env:"API_PATH_PREFIX"`
	AppMode                   string `env:"APP_MODE"`
	AppModeDev                string `env:"APP_MODE_DEV"`
	AppModeProd               string `env:"APP_MODE_PROD"`
	CredentialsPathPrefix     string `env:"CREDENTIALS_PATH_PREFIX"`
	DeleteUserEndpoint        string `env:"DELETE_USER_ENDPOINT"`
	GeoJSONEndpoint           string `env:"GEOJSON_ENDPOINT"`
	GetUserEndpoint           string `env:"GET_USER_ENDPOINT"`
	JWTDomain                 string `env:"JWT_DOMAIN"`
	JWTExpiry                 string `env:"JWT_EXPIRY"`
	JWTSecret                 string `env:"JWT_SECRET"`
	LoginEndpoint             string `env:"LOGIN_ENDPOINT"`
	MapboxAccessToken         string `env:"MAPBOX_ACCESS_TOKEN"`
	MapboxAccessTokenEndpoint string `env:"MAPBOX_ACCESS_TOKEN_ENDPOINT"`
	PostgresAdminPassword     string `env:"POSTGRES_ADMIN_PASSWORD"`
	PostgresDSN               string `env:"POSTGRES_DSN"`
	RegisterEndpoint          string `env:"REGISTER_ENDPOINT"`
	ServerHost                string `env:"SERVER_HOST"`
	ServerMessage             string `env:"SERVER_MESSAGE"`
	ServerPort                string `env:"SERVER_PORT"`
	ServerProtocol            string `env:"SERVER_PROTOCOL"`
	TLSCert                   string `env:"TLS_CERT"`
	TLSKey                    string `env:"TLS_KEY"`
	UpdatePasswordEndpoint    string `env:"UPDATE_PASSWORD_ENDPOINT"`
	ValidateUserEndpoint      string `env:"VALIDATE_USER_ENDPOINT"`
}

func LoadEnv(envFile ...string) (Config, error) {
	cfg := Config{}
	if len(envFile) == 0 {
		envFile = append(envFile, ".env")
	}
	if err := godotenv.Load(envFile[0]); err != nil {
		return cfg, fmt.Errorf(".env load failure: %w", err)
	}
	opts := env.Options{RequiredIfNoDef: true}
	if err := env.ParseWithOptions(&cfg, opts); err != nil {
		return cfg, fmt.Errorf(".env parse failure: %w", err)
	}
	log.Println("\033[36m", ".env load success", "\033[0m")
	return cfg, nil
}
