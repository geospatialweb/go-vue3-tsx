package env

import (
	"encoding/json"
	"strings"
	"testing"
)

func setConfigMap() map[string]string {
	var configMap map[string]string
	e, _ := LoadEnv("../../.env")
	env, _ := json.Marshal(e)
	json.Unmarshal(env, &configMap)
	return configMap
}

func Test_Env_EnvFile(t *testing.T) {
	configMap := setConfigMap()
	expectedEnv := map[string]string{
		"AppMode":                   "development",
		"AppModeDev":                "development",
		"AppModeProd":               "production",
		"ServerMessage":             "HTTP server listening at:",
		"ServerHost":                "localhost",
		"ServerPort":                "8000",
		"ServerProtocol":            "http://",
		"ApiPathPrefix":             "/api",
		"DeleteUserEndpoint":        "/deleteuser",
		"GeoJSONEndpoint":           "/geojson",
		"GetUserEndpoint":           "/getuser",
		"MapboxAccessTokenEndpoint": "/mapbox-access-token",
		"UpdatePasswordEndpoint":    "/updatepassword",
		"CredentialsPathPrefix":     "/credentials",
		"LoginEndpoint":             "/login",
		"RegisterEndpoint":          "/register",
		"ValidateUserEndpoint":      "/validateuser",
	}

	for _, e := range expectedEnv {
		t.Run(e, func(t *testing.T) {
			if !strings.EqualFold(configMap[e], expectedEnv[e]) {
				t.Error("app env variables and expected env variables match failure")
			}
		})
	}
}
