package validation

import (
	"net/http"
	"testing"

	"github.com/go-ozzo/ozzo-validation/v4/is"
)

func Test_Validation_Email(t *testing.T) {
	t.Run("valid email", func(t *testing.T) {
		email := "johncampbell@geospatialweb.ca"
		expectedStatusCode := http.StatusOK
		status, _ := ValidateEmailFormat(email)
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("invalid email", func(t *testing.T) {
		email := "johncampbellgeospatialweb.ca"
		expectedStatusCode := http.StatusBadRequest
		status, _ := ValidateEmailFormat(email)
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}

func Test_Validation_JSON(t *testing.T) {
	t.Run("valid json", func(t *testing.T) {
		json := `{"name": "John Campbell"}`
		expectedStatusCode := http.StatusOK
		status, _ := ValidateJSON([]byte(json))
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("invalid json", func(t *testing.T) {
		json := "{'name': 'John Campbell'}"
		expectedStatusCode := http.StatusInternalServerError
		status, _ := ValidateJSON([]byte(json))
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}

func Test_Validation_RequestURI(t *testing.T) {
	t.Run("valid request URI", func(t *testing.T) {
		requestURI := "/api/geojson?columns=name,description,geom&table=office"
		expectedStatusCode := http.StatusOK
		status, _ := ValidateRequestURI(requestURI)
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})

	t.Run("invalid request URI", func(t *testing.T) {
		requestURI := `\api\geojson?columns=name,description,geom&table=office`
		expectedStatusCode := http.StatusBadRequest
		status, _ := ValidateRequestURI(requestURI)
		if status != expectedStatusCode {
			t.Error("status code and expectedStatusCode match failure")
		}
	})
}

func Test_Validation_Validate(t *testing.T) {
	t.Run("validate", func(t *testing.T) {
		email := "johncampbell@geospatialweb.ca"
		rule := is.EmailFormat
		if err := validate(email, rule); err != nil {
			t.Error(err)
		}
	})
}
