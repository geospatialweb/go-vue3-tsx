package validation

import (
	"fmt"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

func ValidateEmailFormat(email string) (int, error) {
	rule := is.EmailFormat
	if err := validate(email, rule); err != nil {
		return http.StatusBadRequest, fmt.Errorf("email format validation failure: %w", err)
	}
	return http.StatusOK, nil
}

func ValidateJSON(json []byte) (int, error) {
	rule := is.JSON
	if err := validate(json, rule); err != nil {
		return http.StatusInternalServerError, fmt.Errorf("json validation failure: %w", err)
	}
	return http.StatusOK, nil
}

func ValidateRequestURI(requestURI string) (int, error) {
	rule := is.RequestURI
	if err := validate(requestURI, rule); err != nil {
		return http.StatusBadRequest, fmt.Errorf("request URI validation failure: %w", err)
	}
	return http.StatusOK, nil
}

func validate(value interface{}, rule validation.StringRule) error {
	required := validation.Required
	if err := validation.Validate(value, required, rule); err != nil {
		return err
	}
	return nil
}
