package main

import (
	"context"
	"log"
	"os"
	"os/signal"

	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/app"
	"gitlab.com/geospatialweb/vue-go-mvt-postgis/pkg/env"
)

func main() {
	env, err := env.LoadEnv()
	if err != nil {
		log.Fatal(err)
	}
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()
	app := &app.App{
		Ctx: ctx,
		Env: env,
	}
	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}
